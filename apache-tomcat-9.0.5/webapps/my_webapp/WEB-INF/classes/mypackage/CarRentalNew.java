package mypackage;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CarRentalNew extends HttpServlet {

	int cont = 0;
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		String nombre = req.getParameter("name");
		String model = req.getParameter("model_vehicle");
		String engine = req.getParameter("sub_model_vehicle");
		cont ++;
		out.println("<html><big>Hola Amigo "+ nombre + "</big><br>"+
                cont + " Accesos desde su carga.</html>");
    
		JSONParser parser = new JSONParser();
		JSONArray rentals = new JSONArray();
		try (Reader reader = new FileReader("/home/apache-tomcat-9.0.5/rentals.json")) {
			JSONObject jsonObject = (JSONObject) parser.parse(reader);
			rentals = (JSONArray) jsonObject.get("rentals");

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		JSONObject obj = new JSONObject();
		obj.put("model", model);
		obj.put("engine", engine);
		rentals.add(obj);
		try(FileWriter file = new FileWriter("/home/apache-tomcat-9.0.5/rentals.json", true)){
			file.write(rentals.toJSONString());
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException {
		doGet(req, res);
  
	}
}
